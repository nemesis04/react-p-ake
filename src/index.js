import React from "react";
import ReactDOM from "react-dom";
import * as serviceWorker from "./serviceWorker";
import { Provider } from "react-redux";

import App from "./App";
// import configureStore from './js/stores/index'

// const store = configureStore()

ReactDOM.render(
  //   <Provider store={store}>
  <App />,
  //   </Provider>,
  document.getElementById("root")
);
serviceWorker.unregister();
